/***********************************************************************  
# Program name : Convention.js
# Function     : 1. Basic component convention
# Version    Date           Author    Change Description
# 1.0.0      2019-12-14     SH.KIM      Created 
*************************************************************************/
//Import basic module
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

//Import css file
import './.scss';

//Import component
import {} from '.'


@observer
class Convention extends Component {

    constructor() {
        super(...arguments);
    }

    render() {
        return(
            <div className=""></div>
        )
    }
}

Convention.propsTypes = {
};
export default Convention;