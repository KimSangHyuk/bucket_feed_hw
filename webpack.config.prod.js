const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin")
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const outputDirectory = 'dist';

module.exports = {
    mode: 'production',
    // Here the application starts executing and webpack starts bundling
    // entry: './src/index.js', //프론트앤드 사이드 시작 js 파일
    entry : ["@babel/polyfill", './src/index.js'],
    // the target directory and the filename for the bundled output
    output: {
        path: path.join(__dirname, outputDirectory),
        publicPath: '/',
        filename: 'bundle.js'
    },
    /*
        Module loaders are transformations that are applied on the source code
        of a module. We pass all the js file through babel-loader to transform JSX
        to Javascript. CSS files are passed through css-loaders and style-loaders
        to load and bundle CSS files.
    */
    node: {fs: 'empty'},
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(css|scss|sass)$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.(ico|otf|ttf)$/i,
                use: ['url-loader?limit=100000']
            },
            {
                test: /\.(jpe?g|png|gif)$/i,
                use: [
                    // 'file-loader?name=img/[name].[ext]',
                    'file-loader?name=img/[name].[ext]',
                    {
                      loader: 'image-webpack-loader',
                      options: {
                        mozjpeg: {
                            progressive: true,
                            quality: 80
                          },
                          // optipng.enabled: false will disable optipng
                          optipng: {
                            enabled: true,
                          },
                          pngquant: {
                            quality: [0.90, 0.95] ,
                            speed: 4
                          },
                          gifsicle: {
                            interlaced: false
                          },
                      },
                    },
                    // {
                    //     loader: 'file-loader',
                    //     options: {
                    //         // publicPath: 'dist/assets/',
                    //         name: 'img/[name].[ext]',
                    //     }
                    // }
                  ],
            },
        ]
    },
    /*
        clean-webpack-plugin is a webpack plugin to remove the build folder(s) before building.
        html-webpack-plugin simplifies creation of HTML files to serve your webpack bundles.
        It loads the template (public/index.html) and injects the output bundle.
    */
    plugins: [
        new HtmlWebpackPlugin({
            inject: true,
            title: 'Production',
            template: './public/index.html',
            favicon: './public/favicon.ico',
            minify: {
                collapseWhitespace: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true,
            }
        }),
          new ExtractTextPlugin("bundle.css"),
          new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.optimize\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorPluginOptions: {
              preset: ['default', { discardComments: { removeAll: true } }],
            },
            canPrint: true
          }),
          new CleanWebpackPlugin(),
    ]
};
