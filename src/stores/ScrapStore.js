/***********************************************************************  
# Program name : ScrapStore.js
# Function     :1. ScrapArray 및 관련 action들을 관리하는 store
                2. initScrap
                    - 웹 진입시 localstorage에 이전에 스크랩한 스크랩들을 가져옴
                    - 없다면 빈 배열로 초기화
                    - 다시 localStorage에 저장
                3. addScrap
                    - 각 Feed object의 flag 변경
                    - currentScrapArray에 추가
                    - currentScrapArray를 localstorage에 저장
                4. removeScrap
                    - 각 Feed object의 flag 변경
                    - currentScrapArray에서 삭제 (findIndex function으로 object를 찾는다)
                    - currentScrapArray를 localstorage에 저장
                5.
# Version    Date           Author    Change Description
# 1.0.0      2019-12-16     SH.KIM      Created 
*************************************************************************/

//Import basic module
import {observable, action, computed} from 'mobx';
export default class ScrapStore {
    @observable currentScrapArray = [];

    @action data(data) {
        if(data.currentScrapArray) {
            this.currentScrapArray = data.currentScrapArray;
        }
    }

    @action initScrap = () => {
        let prevScrapArray = localStorage.getItem('scrapArray');
        if(prevScrapArray == 'undefined' || prevScrapArray == 'null'){
            prevScrapArray = [];
        } else {
            prevScrapArray = JSON.parse(prevScrapArray);
        }

        this.currentScrapArray = prevScrapArray;
        localStorage.setItem('scrapArray', JSON.stringify(this.currentScrapArray));
    }

    //모든 Scrap을 reset
    @action resetScrap = () => {
        localStorage.setItem('scrapArray', '[]');
    }

    //Scrap 추가 localstorage에 추가
    @action addScrap = (feedObj) => {
        feedObj.scrapFlag = true;
        this.currentScrapArray.push(feedObj);
        localStorage.setItem('scrapArray', JSON.stringify(this.currentScrapArray));
    }

    //Scrap 취소 localstorage에서 삭제
    @action removeScrap = (feedObj) => {
        feedObj.scrapFlag = false;
        let removeIdx = this.currentScrapArray.findIndex(item => item.id === feedObj.id);
        this.currentScrapArray.splice(removeIdx, 1);
        localStorage.setItem('scrapArray', JSON.stringify(this.currentScrapArray));
    }
 
}