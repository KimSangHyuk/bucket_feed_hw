/***********************************************************************  
# Program name : ButtonWidthOpacity.js
# Function     : 1. Button 클릭시 opacity animation
				 2. childComponent, style, onClick prop을 넘겨 받는다.
# Version    Date           Author    Change Description
# 1.0.0      2019-12-15     SH.KIM      Created 
*************************************************************************/
//Import basic module
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Import css file
import './UtilStyle.scss';

class ButtonWidthOpacity extends Component {
	state = {
		touched: false
	}
	
	toggleTouched = () => {
		this.setState( prevState => ({
			touched: !prevState.touched
		}));
	}
	
	handleMouseUp = () => {
		setTimeout( () => {
			this.setState({ touched: false });
		}, 150);
	}
	
	render () {
		const { 
			childComponent,
			style,
			onClick
		} = this.props;
		const { touched } = this.state;
		const className = touched ? 'btn touched' : 'btn';

		return (
			<button 
				className={className}
				style={style}
				onClick={onClick}
				onMouseDown={this.toggleTouched}
				onMouseUp={this.handleMouseUp}
			>
				{childComponent}
			</button>
		)
	}
}

ButtonWidthOpacity.propsTypes = {
	childComponent : PropTypes.element.isRequired,
	style : PropTypes.object,
	onClick : PropTypes.func.isRequired
};
export default ButtonWidthOpacity;