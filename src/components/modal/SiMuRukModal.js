/***********************************************************************  
# Program name : SiMuRukModal.js
# Function     : 1. 스크랩 취소를 확인하면 뜬다.
# Version    Date           Author    Change Description
# 1.0.0      2019-12-16     SH.KIM      Created 
*************************************************************************/
//Import basic module
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

//Import external module
import Modal from 'react-awesome-modal';

//Import Utils
import { ButtonWithOpacity } from "../../utils/index";

//Import css file
import './ModalStyle.scss';

class SiMuRukModal extends Component {

    render() {
        const {
            popupFlag,
            closePopup,
            onClickConfrimScrapCancel
        } = this.props;

        return(
            <Modal 
                visible={popupFlag}
                width="70"
                height="50"
                effect="fadeInUp"
                onClickAway={() => closePopup()}
            >
                <span id="popupText" className="appleSDGothicNeoBold">
                    시무룩
                </span>
            </Modal>
        )
    }
}

SiMuRukModal.propsTypes = {
    popupFlag : PropTypes.bool,
    closePopup : PropTypes.func,
    onClickConfrimScrapCancel : PropTypes.func
};
export default SiMuRukModal;