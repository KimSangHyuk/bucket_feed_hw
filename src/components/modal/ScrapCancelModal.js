/***********************************************************************  
# Program name : ScrapCancelModal.js
# Function     : 1. 스크랩 취소 확인 모달.
                 2. 확인하면 스크랩이 취소되며, localStorage에서 삭제된다.
# Version    Date           Author    Change Description
# 1.0.0      2019-12-16     SH.KIM      Created 
*************************************************************************/
//Import basic module
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

//Import external module
import Modal from 'react-awesome-modal';

//Import Utils
import { ButtonWithOpacity } from "../../utils/index";
import { SiMuRukModal } from "./index";

//Import css file
import './ModalStyle.scss';
@observer
class ScrapCancelModal extends Component {
    @observable smrPopupFlag = false
    render() {
        const {
            popupFlag,
            closePopup,
            onClickConfrimScrapCancel
        } = this.props;

        return(
            <React.Fragment>
                <Modal 
                    visible={popupFlag}
                    width="250"
                    height="100"
                    effect="fadeInUp"
                    onClickAway={() => closePopup()}
                >
                    <span id="popupText" className="appleSDGothicNeoBold">
                        스크랩을 취소할까요? :)
                    </span>
                    <ButtonWithOpacity
                        onClick={() => {
                            onClickConfrimScrapCancel();
                            this.smrPopupFlag = !this.smrPopupFlag;
                            setTimeout(()=>{
                                this.smrPopupFlag = !this.smrPopupFlag;
                            },1000)
                        }}
                        childComponent={
                            <span className="appleSDGothicNeoRegular modalButton"style={{marginLeft:2}}>확인</span>
                        }
                    />
                    <ButtonWithOpacity
                        onClick={() => closePopup()}
                        childComponent={
                            <span className="appleSDGothicNeoRegular modalButton" style={{marginLeft:2}}>취소</span>
                        }
                    />
                </Modal>
                <SiMuRukModal
                    popupFlag={this.smrPopupFlag}
                />
            </React.Fragment>

        )
    }
}

ScrapCancelModal.propsTypes = {
    popupFlag : PropTypes.bool,
    closePopup : PropTypes.func,
    onClickConfrimScrapCancel : PropTypes.func
};
export default ScrapCancelModal;