/***********************************************************************  
# Program name : FeedPhoto.js
# Function     : Feed로 부터 feedInfo를 받아와 Image Component 반환
# Version    Date           Author    Change Description
# 1.0.0      2019-12-15     SH.KIM      Created 
*************************************************************************/
//Import basic module
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable } from 'mobx';
import { observer, inject } from 'mobx-react';
import PropTypes from 'prop-types';

//Import Util
import { ButtonWithOpacity } from "../../utils/index";

//Import Component
import { ScrapCancelModal } from "../modal/index";

//Import css file
import './FeedStyle.scss';

@inject('ScrapStore')
@observer
class FeedPhoto extends Component {
    @observable popupFlag = false;

    //Scrap된 이미지인지 아닌지 구분해서 반환 스크랩 아이콘 반환
    setScrapImg = (feedInfo) => {
        if(feedInfo.scrapFlag){
            return (
                <img alt="" id="feedScrap" src={require('../../../asset/img/scrapOnImg.svg')}/>
            )
        } else {
            return (
                <img alt="" id="feedScrap" src={require('../../../asset/img/scrapOffImg.svg')}/>
            )
        }
    }

    //Scrap, Scrap취소
    onClickScrapButton = (feedInfo) => {
        if(!feedInfo.scrapFlag){
            this.props.ScrapStore.addScrap(feedInfo);
        } else {
            this.openPopup();
        }
    }

    //popup열기
    openPopup = () => {
        this.popupFlag = true;
    }

    //popup닫기
    closePopup = () => {
        this.popupFlag = false;
    }

    //스크랩 취소 확인버튼 온클릭 이벤트
    onClickConfrimScrapCancel = () => {
        const { feedInfo } = this.props;

        this.props.ScrapStore.removeScrap(feedInfo);
        this.closePopup();
    }

    render() {
        const {
            feedInfo
        } = this.props;
        return(
            <div id="feedPhotoWrapper">
                <img alt="" id="feedPhoto" src={feedInfo.image_url}/>
                <ButtonWithOpacity 
                    className="buttonWrapper" 
                    style={{display:'flex', flexDirection:'row'}} 
                    onClick={()=>{this.onClickScrapButton(feedInfo)}}
                    childComponent={this.setScrapImg(feedInfo)}
                />
                <ScrapCancelModal
                    popupFlag={this.popupFlag}
                    closePopup={this.closePopup}
                    onClickConfrimScrapCancel={this.onClickConfrimScrapCancel}
                />
            </div>
        )
    }
}
FeedPhoto.propsTypes = {
    feedInfo : PropTypes.object
};
export default FeedPhoto;