/***********************************************************************  
# Program name : Feed.js
# Function     : 1. 한개의 Feed를 구성한다.
                 2. Feed는 UserInfo, Photo를 child component로 갖고있다.
                 3. FeedUserInfo
                    props: nickname, profileImgUrl 
                 4. FeedPhoto
                    props: feedInfo
# Version    Date           Author    Change Description
# 1.0.0      2019-12-15     SH.KIM      Created 
*************************************************************************/
//Import basic module
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

//Import css file
import './FeedStyle.scss';

//Import component
import {FeedUserInfo, FeedPhoto} from './index'

@observer
class Feed extends Component {
    render() {
        const {
            feedInfo
        } = this.props;
        
        return(
            <span id="feed">
                <FeedUserInfo 
                    feedInfo={feedInfo}
                    nickname={feedInfo.nickname}
                    profileImgUrl={feedInfo.profile_image_url}
                />
                <FeedPhoto feedInfo={feedInfo}/>
            </span>
        )
    }
}

Feed.propsTypes = {
    feedInfo:PropTypes.object
};
export default Feed;