/***********************************************************************  
# Program name : FeedPanel.js
# Function     : 1. Home으로 부터 FeedList를 받아와 뿌려줍니다.
# Version    Date           Author    Change Description
# 1.0.0      2019-12-15     SH.KIM      Created 
*************************************************************************/
//Import basic module
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

//Import css file
import './FeedStyle.scss';

//Import component
import {Feed} from './index'

@observer
class FeedPanel extends Component {

    //FeedList를 하나의 Feed들로 구성하여 반환
    //Feed prop : feedInfo
    setFeed = () => {
        const { feedList } = this.props;
        return (
            feedList.map(
                (feedInfo, idx)=>{
                    return (
                        <Feed key={idx} feedInfo={feedInfo}/>
                    )
                }
            )
        )
    }

    render() {
        return(
            <span style={{height:document.documentElement.clientHeight}}>
                {this.setFeed()}
            </span>
        )
    }
}
FeedPanel.propsTypes = {
    feedList : PropTypes.array
};
export default FeedPanel;