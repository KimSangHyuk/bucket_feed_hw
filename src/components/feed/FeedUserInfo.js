/***********************************************************************  
# Program name : FeedUserInfo.js
# Function     : Feed로 부터 UserInfo(nickname, profileImgUrl)을 받아와 구성 후 반환
# Version    Date           Author    Change Description
# 1.0.0      2019-12-15    SH.KIM      Created 
*************************************************************************/
//Import basic module
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

//Import css file
import './FeedStyle.scss';

@observer
class FeedUserInfo extends Component {
    render() {
        const {
            nickname,
            profileImgUrl
        } = this.props;

        return(
            <React.Fragment>
                <span id="avatarWrapper">
                    <img alt="" src={profileImgUrl}/>
                </span>
                <span id="nickname" className="appleSDGothicNeoBold" style={{fontSize:15}}>
                    {nickname}
                </span>
            </React.Fragment>
        )
    }
}
FeedUserInfo.propsTypes = {
    nickname : PropTypes.string,
    profileImgUrl : PropTypes.string
};
export default FeedUserInfo;