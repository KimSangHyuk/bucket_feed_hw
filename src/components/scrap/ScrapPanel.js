/***********************************************************************  
# Program name : ScrapPanel.js
# Function     :  1. 스크랩 Button panel
# Version    Date           Author    Change Description
# 1.0.0      2019-12-16     SH.KIM      Created 
*************************************************************************/
//Import basic module
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

//Import css file
import './ScrapStyle.scss';

//Import component
import {ScrapButton} from './index'

@observer
class ScrapPanel extends Component {
    render() {
        const {
            onClickOnlyScrapButton,
            buttonClass,
            iconId
        } = this.props;

        return(
            <div className="scrapPanel" style={{display:'flex'}}>
                <ScrapButton 
                    onClickOnlyScrapButton={onClickOnlyScrapButton}
                    buttonClass={buttonClass}
                    iconId={iconId}
                />
            </div>
        )
    }
}

ScrapButton.propsTypes = {
    iconId: PropTypes.string,
    buttonClass: PropTypes.string,
    onClickOnlyScrapButton: PropTypes.func
};
export default ScrapPanel;