/***********************************************************************  
# Program name : ScrapButton.js
# Function     : 1. buttonClass, iconid를 통한 버튼 CSS변경
                 2. 클릭시 스크랩한 것만 보기, 전체 보기 
# Version    Date           Author    Change Description
# 1.0.0      2019-12-16     SH.KIM      Created 
*************************************************************************/
//Import basic module
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable } from 'mobx';
import { observer, inject } from 'mobx-react';
import PropTypes from 'prop-types';

//Import additional module
import { IoIosCheckmark } from "react-icons/io";

//Import Util
import { ButtonWithOpacity } from "../../utils/index";

@inject('ScrapStore')
@observer
class ScrapButton extends Component {    

    //ButtonUtil에 넘기는 child component반환
    scrapChildComponet = () => {
        const {
            buttonClass,
            iconId
        } = this.props;

        return(
            <React.Fragment>
                <div id={buttonClass}><IoIosCheckmark id={iconId}/></div>
                <div className="appleSDGothicNeoRegular" style={{fontSize:15, color:'#424242', marginLeft:6}}>스크랩한 것만 보기</div>
            </React.Fragment>
        )  
    }

    render() {
        const {
            onClickOnlyScrapButton,
        } = this.props;
        return(
            <ButtonWithOpacity 
                className="buttonWrapper" 
                style={{display:'flex', flexDirection:'row'}} 
                onClick={()=>{onClickOnlyScrapButton()}}
                childComponent={this.scrapChildComponet()}
            />
        )
    }
}

ScrapButton.propsTypes = {
    iconId: PropTypes.string,
    buttonClass: PropTypes.string,
    onClickOnlyScrapButton: PropTypes.func
};
export default ScrapButton;