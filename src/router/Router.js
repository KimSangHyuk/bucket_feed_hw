/***********************************************************************  
# Program name : Router.js
# Function     : 1. react-router-dom module을 이용해서 각page로 routing 해줍니다.
#				 2. Switch 내부에 div를 선언할때 나오는 warning을 제거하기위해 Fragment 사용했습니다.
# 				 3. '/' path에 home page 연결
# Version    Date           Author    Change Description
# 1.0.0      2019-12-14     SH.KIM      Created 
*************************************************************************/
//Import basic module
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';

//Import component
import {Home} from '../pages/index';

const Router = () => (
    <BrowserRouter>
        <Switch>
            <React.Fragment>
                <div className="mainContainer"> 
                    <Route exact path="/" component={Home} />
                </div>
            </React.Fragment>
        </Switch>
    </BrowserRouter>
);

export default Router;
