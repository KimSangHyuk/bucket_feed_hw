/***********************************************************************  
# Program name : Home.js
# Function     : 1. mobx를 적용하기 위해 Provider를 사용하였습니다.
#				 2. Provider에 store들을 전달합니다.
# 				 3. Router로 이동
# Version    Date           Author    Change Description
# 1.0.0      2019-12-14     SH.KIM      Created 
*************************************************************************/
//Import basic module
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable, toJS} from 'mobx';
import { observer, inject } from 'mobx-react';
import axios from 'axios';

//Import component
import {ScrapPanel} from '../components/scrap/index';
import {FeedPanel} from '../components/feed/index';

//Import Util

@inject('ScrapStore')
@observer
class Home extends Component {
    //Feed
    @observable pageIdx = 1;
    @observable feedList = [];
    @observable allFeedList;
    
    //Scrap
    @observable onlyScrap = false;
    @observable buttonClass = "onlyScrapButton";
    @observable iconId = "scrapCheckIcon";

    //localStorage에 들어있는 scrapbook으로 current scrapbook을 초기화
    componentWillMount = () => {
        this.props.ScrapStore.initScrap();
    }

    constructor() {
        super(...arguments);
        this.callFeedList();
    }

    //Scroll event listener 적용
    componentDidMount = () => {
        window.addEventListener('scroll', this.infiniteScroll, true);
    }

    //Api를 Call 
    callFeedList = () => {
        const feedListAPI = "https://s3.ap-northeast-2.amazonaws.com/bucketplace-coding-test/cards/page_" + this.pageIdx + ".json";
        return axios.get(feedListAPI,  { crossdomain: true })
        .then((res) => {
            this.setFeed(res);
        })
        .catch((error) => {
            if(error.response.status == '403'){

            } else {
                console.log(error)
            }
        })
    }

    //Feed List를 call하여 feedList에 넣어준다.
    //빈 Array가 response되면 넣지 않는다.
    setFeed = (res) => {
        if(res.data.length != 0){
            for(var i = 0; i < res.data.length; i++){
                const feedData = res.data[i];
                const currentScrapArray = toJS(this.props.ScrapStore.currentScrapArray);
                const scrapIdx = currentScrapArray.findIndex(item => item.id === feedData.id)

                if(scrapIdx > -1){
                    feedData.scrapFlag = true;
                } else {
                    feedData.scrapFlag = false;
                }
                this.feedList.push(feedData);
                this.allFeedList = this.feedList;
            }
        }
    }

    //ScrollTop + 클라이언트높이 = scroll높이 이면 스크롤의 끝에 다다른것으로 판단
    //끝에 다다르면 새로운 API를 call한다.
    infiniteScroll = () => {
        let scrollHeight = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight);
        let scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop);
        let clientHeight = document.documentElement.clientHeight;

        if(scrollTop + clientHeight === scrollHeight){
            this.pageIdx += 1;
            this.callFeedList();
        }
    }

    //스크랩한 것만 보기 버튼 눌렀을때, CSS class 변경, flag 변경
    onClickOnlyScrapButton = () =>{
        const currentScrapArray = toJS(this.props.ScrapStore.currentScrapArray);
        if(this.onlyScrap){
            this.buttonClass = "onlyScrapButton";
            this.iconId = "scrapCheckIcon";
            this.feedList = this.allFeedList;
        } else {
            this.buttonClass = "onlyScrapButtonSelected";
            this.iconId = "scrapCheckIconSelected";
            this.feedList = currentScrapArray;

        }
        this.onlyScrap = !this.onlyScrap;
    }

    render() {
        return(
            <div style={{display:'flex', flexDirection:'row'}}>
                <div style={{flex:59}}/>
                <div style={{display:'flex', flex:1138, flexDirection:'column'}}>
                    {/* 스크랩한 것만 보기 패널 */}
                    <ScrapPanel 
                        onClickOnlyScrapButton={this.onClickOnlyScrapButton}
                        buttonClass={this.buttonClass}
                        iconId={this.iconId}
                    />

                    {/* Feed전체 패널 */}
                    <FeedPanel feedList={this.feedList}/>
                </div>
                <div style={{flex:59}}/>
            </div>
        )
    }
}
export default Home;