/***********************************************************************  
# Program name : index.js
# Function     : 1. mobx를 적용하기 위해 Provider를 사용하였습니다.
#				 2. Provider에 store들을 전달합니다.
# 				 3. Router로 이동
# Version    Date           Author    Change Description
# 1.0.0      2019-12-14     SH.KIM      Created 
*************************************************************************/

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import Router from './router/Router';
import ScrapStore from './stores/ScrapStore';
import * as serviceWorker from './serviceWorker';
import cors from 'cors';

const scrapStore = new ScrapStore();
ReactDOM.render(
		<Provider
			ScrapStore={scrapStore}
		>
			<Router />
		</Provider>,
	document.getElementById('root'),
);

module.hot.accept();
serviceWorker.register();
